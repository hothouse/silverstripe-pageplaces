<?php

class GooglePlacesTask extends BuildTask {

	protected $description = 'Refreshes the places data with recent data from Google Places, based on the saved Google Places Reference ID.';

	public function run($request) {
		$PagePlaces = PagePlace::get()
			->filter('LastEdited:LessThan', date('Y-m-d', time() - Config::inst()->get('GooglePlacesTask', 'data_refresh_time_offset')))
			->exclude('GooglePlacesReference', '')
			->limit(Config::inst()->get('GooglePlacesTask', 'data_refresh_limit'));

		foreach($PagePlaces as $PagePlace) {
			$PagePlace->write();
		}

		echo 'finished saving '.$PagePlaces->Count().' place records';
	}
}