<?php

/**
 * A utility class for geocoding addresses using the google maps API.
 *
 * @package silverstripe-addressable
 */
class GoogleGeocoding {

	/**
	 * Convert an address into a latitude and longitude.
	 *
	 * @param  string $address The address to geocode.
	 * @param  string $region An optional two letter region code.
	 * @return object A location object.
	 */
	const API_URL_POINTS = 'https://maps.googleapis.com/maps/api/geocode/xml';

	public static function address_to_point($address, $region = null) {
		if (!($key = Config::inst()->get('GoogleGeocoding', 'api_key'))) {
			throw new ValidationException(new ValidationResult(false, 'Please specify a Google Places API Key.'));
		}

		$service = new RestfulService(self::API_URL_POINTS);
		$service->setQueryString(
			array(
				'address' => $address,
				'sensor' => 'false',
				'region' => $region,
				'key' => $key
			)
		);
		$response = $service->request()->simpleXML();

		if ($response->status != 'OK') {
			return false;
		}

		return $response->result->geometry->location;
	}

	/**
	 * Convert a query into a place object.
	 *
	 * @param  string $query The search query.
	 * @return object A places object.
	 */
	const API_URL_PLACES = 'https://maps.googleapis.com/maps/api/place/textsearch/xml';

	public static function search_places($query) {
		if (!($key = Config::inst()->get('GoogleGeocoding', 'api_key'))) {
			throw new ValidationException(new ValidationResult(false, 'Please specify a Google Places API Key.'));
		}

		$querySuffix = ', New Zealand';
		$service = new RestfulService(self::API_URL_PLACES);
		$service->setQueryString(
			array(
				'sensor' => 'false',
				'key' => $key,
				'query' => $query.$querySuffix
			)
		);
		$response = $service->request()->simpleXML();

		if ($response->status != 'OK') {
			return false;
		}

		return $response->result;
	}

	/**
	 * Get place object details.
	 *
	 * @param  string $reference The place reference.
	 * @return object A places object.
	 */
	const API_URL_PLACE_DETAILS = 'https://maps.googleapis.com/maps/api/place/details/xml';

	public static function get_place_details($reference) {
		if (!($key = Config::inst()->get('GoogleGeocoding', 'api_key'))) {
			throw new ValidationException(new ValidationResult(false, 'Please specify a Google Places API Key.'));
		}

		$service = new RestfulService(self::API_URL_PLACE_DETAILS);
		$service->setQueryString(
			array(
				'sensor' => 'false',
				'reference' => $reference,
				'key' => $key
			)
		);
		$response = $service->request()->simpleXML();

		if ($response->status != 'OK') {
			return false;
		}

		return $response->result;
	}

	/**
	 * Get place photo.
	 *
	 * @param  string $reference The photo reference.
	 * @param  string $width The photo width.
	 * @return object A photo url.
	 */
	const API_URL_PHOTO = 'https://maps.googleapis.com/maps/api/place/photo';

	public static function get_place_photo($reference, $width = 100) {
		if (!($key = Config::inst()->get('GoogleGeocoding', 'api_key'))) {
			throw new ValidationException(new ValidationResult(false, 'Please specify a Google Places API Key.'));
		}

		$service = new RestfulService(self::API_URL_PHOTO);
		$service->setQueryString(
			array(
				'sensor' => 'false',
				'photoreference' => $reference,
				'maxwidth' => $width,
				'key' => $key
			)
		);

		$response = $service->request();

		if ($response->getStatusCode() != 200) {
			return false;
		}

		return $response->getBody();
	}

	/**
	 * Get street view image.
	 * Note: no API key needed, therefore no checking here
	 *
	 * @param  string $lat Lat.
	 * @param  string $lng Lng.
	 * @param  string $width The image width.
	 * @param  string $height The image height.
	 * @return object A image url.
	 */
	const API_URL_STREETVIEW = 'http://maps.googleapis.com/maps/api/streetview';

	public static function get_streetview_image($lat, $lng, $width = 600, $height = 600) {

		$service = new RestfulService(self::API_URL_STREETVIEW);
		$service->setQueryString(
			array(
				'sensor' => 'false',
				'location' => $lat.','.$lng,
				'size' => $width.'x'.$height,
				'key' => Config::inst()->get('GoogleGeocoding', 'api_key')
			)
		);

		$response = $service->request();

		if ($response->getStatusCode() != 200) {
			return false;
		}

		return $response->getBody();
	}

}
