<?php

class MapService {

	public function renderTemplate() {
		$Requirements = array(
			MODULE_PAGEPLACES_PATH.'/javascript/markerclusterer.min.js',
			MODULE_PAGEPLACES_PATH.'/javascript/tmpl.js',
			MODULE_PAGEPLACES_PATH.'/javascript/script.js'
		);
		if(Config::inst()->get('MapService', 'use_infobubble') === true) {
			$Requirements[] = MODULE_PAGEPLACES_PATH.'/javascript/infobubble.min.js';
		}
		if(Config::inst()->get('MapService', 'use_markerlabel') === true) {
			$Requirements[] = MODULE_PAGEPLACES_PATH.'/javascript/markerwithlabel.js';
		}
		if(Config::inst()->get('MapService', 'use_infosidebar') === true) {
			Requirements::customScript(<<<JS
$(function() {
	infoSidebarSelector = '.map-infosidebar';
});
JS
			);
		}
		if(Config::inst()->get('MapService', 'use_marker_spiderfier') === true) {
			$Requirements[] = MODULE_PAGEPLACES_PATH.'/javascript/OverlappingMarkerSpiderfier.min.js';
		}
		Requirements::css(MODULE_PAGEPLACES_PATH.'/css/maps.css');
		$GoogleBrowserKey = Config::inst()->get('GoogleGeocoding', 'browser_key');
		Requirements::javascript(sprintf(
			'https://maps.google.com/maps/api/js?libraries=places%s',
			$GoogleBrowserKey ? '&key='.$GoogleBrowserKey : ''
		));
		Requirements::combine_files('maps.js', $Requirements);

		// get icons
		$Theme = Config::inst()->get('SSViewer', 'theme');
		$RetinaIcons = Config::inst()->get('MapService', 'retina_icons') === true;

		// marker icon
		$MarkerFileName = Config::inst()->get('MapService', 'marker_file');
		$MarkerFile = THEMES_PATH.'/'.$Theme.$MarkerFileName;
		if(file_exists($MarkerFile) && $Size = getimagesize($MarkerFile)) {
			if($Size[2] != IMAGETYPE_PNG) {
				user_error('Map markers must be PNG type images.', E_USER_NOTICE);
			} else {
				$MarkerFile = THEMES_DIR.'/'.$Theme.$MarkerFileName;
				$Width = $RetinaIcons ? floor($Size[0]/2) : $Size[0];
				$Height = $RetinaIcons ? floor($Size[1]/2) : $Size[1];
				Requirements::customScript(<<<JS
$(function() {
	markerIcon = {
		url: '$MarkerFile',
		scaledSize: new google.maps.Size({$Width}, {$Height}),
		origin: new google.maps.Point(0,0),
		anchor: new google.maps.Point(Math.floor({$Width}/2), {$Height})
	};
});
JS
);
			}
		}

		// cluster icon
		$MarkerClusterFileName = Config::inst()->get('MapService', 'marker_cluster_file');
		$MarkerClusterFile = THEMES_PATH.'/'.$Theme.$MarkerClusterFileName;
		if(file_exists($MarkerClusterFile) && $Size = getimagesize($MarkerClusterFile)) {
			if($Size[2] != IMAGETYPE_PNG) {
				user_error('Map markers must be PNG type images.', E_USER_NOTICE);
			} else {
				$MarkerClusterFile = THEMES_DIR.'/'.$Theme.$MarkerClusterFileName;
				$Width = $RetinaIcons ? floor($Size[0]/1.25/2) : floor($Size[0]/1.25);
				$Height = $RetinaIcons ? floor($Size[1]/1.25/2) : floor($Size[1]/1.25);
				$Width2 = $RetinaIcons ? floor($Size[0]/2) : $Size[0];
				$Height2 = $RetinaIcons ? floor($Size[1]/2) : $Size[1];
				Requirements::customScript(<<<JS
$(function() {
	markerClustererOptions = {
		styles: [{
			url: '$MarkerClusterFile',
			width: {$Width},
			height: {$Height},
			anchorIcon: [{$Height}, Math.floor({$Width}/2)],
			textColor: '#ffffff',
			textSize: 12
		}, {
			url: '$MarkerClusterFile',
			width: {$Width2},
			height: {$Height2},
			anchorIcon: [{$Height2}, Math.floor({$Width2}/2)],
			textColor: '#ffffff',
			textSize: 12
		}]
	};
});
JS
);
				Requirements::customCSS(<<<JS
.ie8 .cluster, .ie7 .cluster {
	background: none!important;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$MarkerClusterFile', sizingMethod='scale');
}
JS
);
			}
		}

		return Controller::curr()->renderWith('MapCanvas');
	}

}