<?php

class PagePlacesExtension extends DataExtension {

	public $mapService;
	public static $dependencies = array(
		'mapService'	=> '%$MapService',
	);

	private static $has_many = array(
		'PagePlaces' => 'PagePlace'
	);

	public function updateCMSFields(FieldList $fields) {
		$gridFieldConfig = GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldAddNewButton('toolbar-header-right'),
			new GridFieldSortableHeader(),
			new GridFieldDataColumns(),
			new GridFieldPaginator(20),
			new GridFieldEditButton(),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm()
		);
		$gridField = new GridField('PagePlaces', 'PagePlaces', $this->owner->PagePlaces(), $gridFieldConfig);
		$fields->addFieldToTab('Root.Places', $gridField);
	}

	public function Map() {
		return $this->mapService->renderTemplate();
	}

	public function Places() {
		return $this->owner->PagePlaces();
	}

}
