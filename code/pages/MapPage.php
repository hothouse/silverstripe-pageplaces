<?php

class MapPage extends Page {

}

class MapPage_Controller extends Page_Controller {

	public $mapService;
	public static $dependencies = array(
		'mapService'	=> '%$MapService'
	);

	public function Map() {
		return $this->mapService->renderTemplate();
	}

	public function Places() {
		$stage = Versioned::current_stage();
		$suffix = (!$stage || $stage == 'Stage') ? "" : "_$stage";
		$Places = PagePlace::get()->innerJoin("SiteTree$suffix", "\"SiteTree$suffix\".\"ID\" = \"PagePlace\".\"PageID\"");
		if(class_exists('Translatable') && $this->dataRecord->hasExtension('Translatable')) {
			$Places = $Places->filter('Locale', Translatable::get_current_locale());
		}
		return $Places;
	}

	public function MapPagePlaces() {
		return $this->PagePlaces();
	}

}