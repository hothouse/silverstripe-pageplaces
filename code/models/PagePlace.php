<?php

class PagePlace extends DataObject {

	private static $folder_name = 'Places';

	private static $db = array(
		'Title' => 'Varchar(255)',
		'Content' => 'HTMLText',
		// everything below will be always fed from google places
		// except address, that will be available if we are not using google places
		'Address' => 'Text',
		'Lat' => 'Varchar',
		'Lng' => 'Varchar',
		'GooglePlacesReference' => 'Varchar(255)',
		'PhoneNumber' => 'Varchar',
		'Website' => 'Varchar(255)',
		'OpeningHours' => 'Text'
	);

	private static $summary_fields = array(
		'Title' => 'Title',
		'Address' => 'Address',
		'Lat' => 'Lat',
		'Lng' => 'Lng'
	);

	private static $has_one = array(
		'Image' => 'Image',
		'Page' => 'Page'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName('PageID');
		$fields->removeByName('Locale');

		// if we dont use a google place
		if(!$this->GooglePlacesReference) {
			// hide it
			$fields->addFieldToTab('Root.Main', HiddenField::create('GooglePlacesReference'));
		} else {
			// else hide additional fields
			$fields->replaceField('Title', $fields->dataFieldByName('Title')->performReadonlyTransformation());
			$fields->replaceField('GooglePlacesReference', $fields->dataFieldByName('GooglePlacesReference')->performReadonlyTransformation());
			$fields->replaceField('Address', $fields->dataFieldByName('Address')->performReadonlyTransformation());
			foreach (array('GooglePlacesReference', 'Address', 'Title', 'Website', 'PhoneNumber') as $key) {
				$fields->replaceField($key, $fields->dataFieldByName($key)->performReadonlyTransformation());
			}
		}

		// if not yet saved
		if(!$this->ID) {
			// add places autocompleter
			if(Config::inst()->get('GoogleGeocoding', 'api_key')) {
				$fields->addFieldToTab('Root.Main', GooglePlacesAutocompleteField::create('GooglePlacesAutocomplete', 'Search for a place on Google'), 'Title');
				$fields->addFieldToTab('Root.Main', LiteralField::create('or', '<strong> or enter a custom location below</strong><br /><br />'), 'Title');
			}

			// and hide additional fields
			$fields->addFieldToTab('Root.Main', HiddenField::create('Lat'));
			$fields->addFieldToTab('Root.Main', HiddenField::create('Lng'));
			$fields->removeByName('Content');
			$fields->removeByName('Image');
		} else {
			// image including folder name
			$fields->addFieldToTab("Root.Main", UploadField::create('Image')->setFolderName(self::$folder_name));
		}

		// always remove
		foreach (array('Lat', 'Lng') as $key) {
			$fields->replaceField($key, $fields->dataFieldByName($key)->performReadonlyTransformation());
		}
		$fields->removeByName('OpeningHours');

		return $fields;
	}

	public function onBeforeWrite() {
		// parent
		parent::onBeforeWrite();

		// add locale if needs be
		if(class_exists('Translatable') && $this->Page()->hasExtension('Translatable')) {
			$this->Locale = $this->Page()->Locale;
		}

		// get details if we use a places reference
		if($this->GooglePlacesReference) {
			if(!$PlaceDetails = GoogleGeocoding::get_place_details($this->GooglePlacesReference)) {
				return;
			}

			// save details
			$this->PhoneNumber = $this->config()->get('use_google_places_international_phone_number') ? (string)$PlaceDetails->international_phone_number : (string)$PlaceDetails->formatted_phone_number;
			$this->Website = (string)$PlaceDetails->website;

			// opening hours
			unset($PlaceDetails->opening_hours->open_now);
			$this->OpeningHours = $PlaceDetails->opening_hours->asXML();

			// photo
			if(!($Photo = $PlaceDetails->photo)) {
				$Photo = $PlaceDetails->photos[0];
			}

			// geometry
			if(isset($PlaceDetails->geometry) && isset($PlaceDetails->geometry->location)) {
				$this->Lat = (float)$PlaceDetails->geometry->location->lat;
				$this->Lng = (float)$PlaceDetails->geometry->location->lng;
			}

			// photo
			if(isset($Photo->photo_reference) && $this->config()->get('attach_google_places_image_if_empty') === true) {
				$this->attachGooglePlacesPhoto($Photo->photo_reference);
			} elseif(!$this->Image()->exists() && $this->config()->get('attach_google_streetview_image_if_empty') === true && $this->Lat && $this->Lng) {
				// get street view image if there is none saved
				$this->attachGoogleStreetViewImage($this->Lat, $this->Lng);
			}

			// no further changes
			return;
		}

		// throw error if nothing specified
		if(!$this->Title && !$this->GooglePlacesReference) {
			throw new ValidationException(new ValidationResult(false, 'Please specify a location.'));
		}

		// get lat/lng for custom address
		if($this->Address) {
			if(!$Location = GoogleGeocoding::address_to_point($this->Address)) {
				return;
			}

			$this->Lat = (float)$Location->lat;
			$this->Lng = (float)$Location->lng;
		}

		// get street view image if there is none saved
		if(!$this->Image()->exists() && $this->config()->get('attach_google_streetview_image_if_empty') === true && $this->Lat && $this->Lng && Config::inst()->get('GoogleGeocoding', 'api_key')) {
			$this->attachGoogleStreetViewImage($this->Lat, $this->Lng);
		}
	}

	public function ContentXML() {
		return Convert::raw2xml($this->obj('Content')->forTemplate());
	}

	public function ImageLink() {
		if($this->Image()->exists() && $Image = $this->Image()->CroppedImage($this->config()->get('image_width'), $this->config()->get('image_height'))) {
			return $Image->Link();
		}
	}

	public function OpeningHoursJSON() {
		$OpeningHours = simplexml_load_string($this->OpeningHours);
		return json_encode($OpeningHours);
	}

	public function attachGooglePlacesPhoto($GooglePlacesPhotoReference) {
		$Size = $this->config()->get('image_width');
		if($PhotoFile = GoogleGeocoding::get_place_photo((string)$GooglePlacesPhotoReference, $Size)) {
			$this->attachImage($PhotoFile);
		}
	}

	public function attachGoogleStreetViewImage($Lat, $Lng) {
		if($PhotoFile = GoogleGeocoding::get_streetview_image($Lat, $Lng, $this->config()->get('image_width'), $this->config()->get('image_height'))) {
			$this->attachImage($PhotoFile);
		}
	}

	public function attachImage($PhotoFile) {
		if(!function_exists('exif_imagetype')) {
			return;
		}

		// create image object in folder
		$Folder = Folder::find_or_make(self::$folder_name);

		// delete old object
		if($this->Image() && $this->Image()->exists()) {
			$this->Image()->delete();
		}

		// delete all other objects with that ID
		$Files = Image::get()->filter(array(
			'ParentID' => $Folder->ID,
			'Name:StartsWith' => $this->ID.'.'
		));
		foreach($Files as $File) {
			$File->delete();
		}

		// creat new object
		$Image = Image::create();
		$Image->setParentID($Folder->ID);
		$Image->setName($this->ID);
		$Image->Title = $this->Title;
		//$Image->write();

		// save image to file
		$ImagePath = $Image->getFullPath();
		file_put_contents($ImagePath, $PhotoFile);

		// adjust name
		switch (exif_imagetype($ImagePath)) {
			case IMAGETYPE_GIF:
				$ImagePathNew = $ImagePath.'.gif';
				break;
			case IMAGETYPE_JPEG:
				$ImagePathNew = $ImagePath.'.jpg';
				break;
			case IMAGETYPE_PNG:
				$ImagePathNew = $ImagePath.'.png';
				break;
			default:
				break;
		}
		rename($ImagePath, $ImagePathNew);

		// re set name
		$Image->setName(basename($ImagePathNew));
		$Image->write();

		// assign image id
		$this->ImageID = $Image->ID;
	}

}
