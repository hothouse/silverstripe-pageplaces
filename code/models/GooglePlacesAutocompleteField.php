<?php

class GooglePlacesAutocompleteField extends TextField {
	public function __construct($name, $title = null, $value = null) {
		parent::__construct($name, $title, $value);
		$this->addExtraClass('text');
		$this->setAttribute('data-key', Config::inst()->get('GoogleGeocoding', 'browser_key'));
	}

	public function FieldHolder($properties = array()) {
		Requirements::javascript(THIRDPARTY_DIR.'/jquery-livequery/jquery.livequery.min.js');
		Requirements::javascript(MODULE_PAGEPLACES_PATH.'/javascript/GooglePlacesAutocompleteField.js');
		return parent::FieldHolder($properties);
	}

}