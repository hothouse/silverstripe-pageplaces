
// map
var mc, map, oms, markersToLoad = 0, allMarkers = {},
	mapOptions = mapOptions || {}, infoBubbleStyle = infoBubbleStyle || {minWidth: 350, minHeight: 100},
	markerIcon, markerClustererOptions = markerClustererOptions || {}, useInfoSidebar, $mapsidebar;
var dayNames = [];
dayNames[dayNames.length] = "Sunday";
dayNames[dayNames.length] = "Monday";
dayNames[dayNames.length] = "Tuesday";
dayNames[dayNames.length] = "Wednesday";
dayNames[dayNames.length] = "Thursday";
dayNames[dayNames.length] = "Friday";
dayNames[dayNames.length] = "Saturday";

$(window).on("load", function() {
    createMapMarker = function(item, template) {
        var
            useMarkerLabel = typeof MarkerWithLabel != 'undefined',
            markerOptions = {
                position: new google.maps.LatLng(parseFloat(item.latitude), parseFloat(item.longitude)),
                icon: markerIcon || null,
                bounds: true,
                map: map,
                animation: google.maps.Animation.DROP,
                id: item.PlaceID,
                content: tmpl(template, item),
                labelContent: item.PlaceTitle
            },
            marker = useMarkerLabel ? new MarkerWithLabel(markerOptions) : new google.maps.Marker(markerOptions);

        // add to map
        if(useOMS) {
            oms.addMarker(marker);
        } else {
            (function(marker) {
                google.maps.event.addListener(marker, 'click', function() {
                    if(useInfoBubble) {
                        infoBubble.setContent(marker.content);
                        infoBubble.open(map, marker);
                    }
                    if(useInfoSidebar) {
                        $mapsidebar.html(marker.content).show();
                    }
                });
            })(marker);
        }

        // add to clusterer
        mc.addMarker(marker);
        allMarkers[item.PlaceID] = marker;

        // add to bounds & center map
        if(mapOptions.doFitBounds) {
            markersToLoad --;
            if(item.latitude && item.longitude) {
                bounds.extend(marker.position);
            }
            if(markersToLoad == 0) {
                // Don't zoom in too far on only one marker
                if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
                    var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
                    var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01);
                    bounds.extend(extendPoint1);
                    bounds.extend(extendPoint2);
                }

                map.fitBounds(bounds);
            }
        } else if(typeof mapOptions.activateMapMarkerIndex == 'number') {
            markersToLoad --;
            if(markersToLoad == 0) {
                var marker = allMarkers[mapOptions.activateMapMarkerIndex];
                google.maps.event.trigger(marker, 'click');
                map.panTo(marker.getPosition());
            }
        }
        if(markersToLoad == 0) {
            delete allMarkers;
        }
    }

    // get map canvas
    var $MapCanvas = $(".MapCanvas");
    if ($MapCanvas.length != 1) {
        return;
    }

    // map style
    var mapCenter = new google.maps.LatLng(-41, 173);
    mapOptions = $.extend({
        zoom: 7,
        center: mapCenter,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        doFitBounds: true
    }, mapOptions);

    // map
    map = new google.maps.Map($MapCanvas[0], mapOptions);

    // fit bounds when finished
    if(mapOptions.doFitBounds) {
        var bounds = new google.maps.LatLngBounds();
    }

    // bubble
    useInfoBubble = typeof InfoBubble === 'function';
    if(useInfoBubble) {
        infoBubbleStyle = $.extend({
            map: map
        }, infoBubbleStyle);
        infoBubble = new InfoBubble(infoBubbleStyle);
    }
    useInfoSidebar = typeof infoSidebarSelector != 'undefined' && infoSidebarSelector != '';
    if(useInfoSidebar) {
        $mapsidebar = $(infoSidebarSelector);
    }

    // spider
    useOMS = typeof OverlappingMarkerSpiderfier === 'function';
    if(useOMS) {
        oms = new OverlappingMarkerSpiderfier(map,{
            markersWontMove: true,
            markersWontHide: true,
            keepSpiderfied: true
        });
        oms.addListener('click', function(marker) {
            if(useInfoBubble) {
                infoBubble.setContent(marker.content);
                infoBubble.open(map, marker);
            }
            if(useInfoSidebar) {
                $mapsidebar.html(marker.content).show();
            }
        });
    }

    // cluster the markers
    markerClustererOptions = $.extend({
        gridSize: 40,
        maxZoom: 17
    }, markerClustererOptions);
    mc = new MarkerClusterer(map, [], markerClustererOptions);

    // get data & prepare
    var markerObjects = [];
    $('.pagePlaces[itemscope]').each(function(index, item) {
        var markerObject = {};
        $(this).children().each(function(index, item) {
            var
                $this = $(this),
                content = $this.attr('content'),
                itemProp = $this.attr('itemprop');

            if(itemProp) {
                markerObject[itemProp] = content;
            }
            if(itemProp == 'OpeningHours') {
                markerObject[itemProp] = JSON.parse(content);
            }
        });

        markerObjects.push(markerObject);
    });

    // create markers
    markersToLoad = markerObjects.length;
    $(markerObjects).each(function(index, item) {
        // this is just to reference how the data fetching from google using js works, not used anymore
        if(0 && item.reference) {
            service = new google.maps.places.PlacesService(map);
            service.getDetails({
                reference: item.reference
            }, function (place, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    createMapMarker(place, 'infoWindowTemplatePlaces');
                }
            });
        } else {
            createMapMarker(item, 'infoWindowTemplate');
        }
    });

    // add listener to close bubble on click
    google.maps.event.addListener(map, 'click', function () {
        if(useInfoBubble) {
            infoBubble.close();
        }
    });

    // trigger finished
    $(window).trigger('mapmarkersloaded');
});

