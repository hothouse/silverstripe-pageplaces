(function($) {
	$('.field.googleplacesautocomplete input').livequery(function() {
		var
			input = this,
			key = $(input).attr('data-key'),
			keyparam = key ? '&key=' + key : '';
		$.getScript(
			'https://www.google.com/jsapi',
			function() {
				google.load('maps', '3', {
					other_params: 'libraries=places' + keyparam,
					callback : function(){
						var autocomplete = new google.maps.places.Autocomplete(input);
						google.maps.event.addListener(autocomplete, 'place_changed', function() {
							var place = autocomplete.getPlace();
							if (!place.geometry) {
								alert('not found');
								return false;
							}

							$('input[name=Title]').val(place.name);
							$('textarea[name=Address]').val(place.formatted_address);
							$('input[name=GooglePlacesReference]').val(place.reference);
							$('input[name=Lat]').val(place.geometry.location.lat());
							$('input[name=Lng]').val(place.geometry.location.lng());
							$('button[name=action_doSave]').trigger('click');
						});
					}
				});
			}
		)
	});
}(jQuery));